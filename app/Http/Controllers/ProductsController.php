<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(){
        $products = Product::all();
        return view('products.index', compact('products'));
    }

    public function create(){
        return view('products.create');
    }

    public function edit($id){
        return view('products.edit', compact('id'));
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'buy_price' => 'required',
            'sale_price' => 'required'
        ]);
        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->buy_price = $request->buy_price;
        $product->sale_price = $request->sale_price;
        $product->save();

        return redirect()->back()->with('success', 'Producto registrado exitosamente');
    }

    public function update(Request $request){
        dd($request);
    }
}
