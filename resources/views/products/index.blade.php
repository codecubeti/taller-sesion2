@extends('master')

@section('content')
    <h1>Inicio</h1>

    <table class="table-striped">
        <thead>
            <th>ID</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Precio compra</th>
            <th>Precio venta</th>
            <th>Estatus</th>
            <th>Editar</th>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>{{$product->id}}</td>
                <td>{{$product->name}}</td>
                <td>{{$product->description}}</td>
                <td>{{$product->buy_price}}</td>
                <td>{{$product->sale_price}}</td>
                <td>{{$product->status ? 'Activo' : 'Inactivo'}}</td>
                <td><a href="{{url('/edit/' . $product->id)}}"><button class="btn">Editar</button> </a> </td>

            </tr>
        @endforeach
        </tbody>
    </table>


@endsection