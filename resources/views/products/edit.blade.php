@extends('master')

@section('content')
    <h1>Editar</h1>

    {{Form::open(['url' => 'store', 'method' => 'post'])}}
        <div class="form-group">
            <input type="hidden" class="form-control" id="id" name="id" value={{$id}}>
        </div>
        @include('products.fragments.inputs')
        <div class="form-check">
            <label class="form-check-label" for="status">Estatus</label>
            <input type="checkbox" class="form-check-input" id="status" name="status">
        </div>
        <button type="submit" class="btn-info">Editar</button>
    {{Form::close()}}
@endsection