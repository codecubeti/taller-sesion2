<div class="form-group">
    <label for="name">Nombre</label>
    <input type="text" class="form-control" id="name" name="name" placeholder="Nombre">
</div>

<div class="form-group">
    <label for="description">Descripción</label>
    <textarea type="text" class="form-control" id="description" name="description" placeholder="Descripción"></textarea>
</div>

<div class="form-group">
    <label for="buy_price">Precio de compra</label>
    <input type="text" class="form-control" id="buy_price" name="buy_price" placeholder="Precio de compra">
</div>

<div class="form-group">
    <label for="sale_price">Precio de venta</label>
    <input type="text" class="form-control" id="sale_price" name="sale_price" placeholder="Precio de venta">
</div>
