@extends('master')

@section('content')
    <h1>Crear</h1>


    {{Form::open(['url' => 'store', 'method' => 'post'])}}
        @include('products.fragments.inputs')
        <button type="submit" class="btn-success">Crear</button>
    {{Form::close()}}

@endsection